﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class exitGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    // the escape key is used to close the application
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
