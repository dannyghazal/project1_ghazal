﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveShip : MonoBehaviour
{
    // public speed class so that the speed of the ship is editable within Unity
    public float speed;

    private Rigidbody2D rbd2;

    // Start is called before the first frame update
    void Start()
    {
        rbd2 = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame before any physics calculations
    void FixedUpdate()
    {
        // grabs player input through the keyboard
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //adds force for movement so when the player uses move keys the game object can move vertically and horizontally
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);
        rbd2.AddForce(movement);


        // if statements provide key commands for shift + WASD to move exactly 1 unit (1 meter) in any given direction
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (Input.GetKey(KeyCode.RightShift))
            {
                transform.position += Vector3.left;
            }
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            if (Input.GetKey(KeyCode.RightShift))
            {
                transform.position += Vector3.right;
            }
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (Input.GetKey(KeyCode.RightShift))
            {
                transform.position += Vector3.up;
            }
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            if (Input.GetKey(KeyCode.RightShift))
            {
                transform.position += Vector3.down;
            }
        }

    }
}
