﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disableMovement : MonoBehaviour
{
    public Vector3 FreezePosition { get; private set; }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      // when the key p is pressed the velocity is set to 0 which freezes the sprites position
        if (Input.GetKey(KeyCode.P))
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }
}
