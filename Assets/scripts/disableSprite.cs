﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disableSprite : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    // When the Q key is used it disables the sprite and the sprite renderer so that the space ship is disabled and can no longer be seen
        if (Input.GetKey(KeyCode.Q))
        {
            gameObject.GetComponent<Rigidbody2D>().GetComponent<Renderer>().enabled = false;
        }
    }
}
