﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class resetPosition : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
    // When the space Key is pressed the the position of the sprite moves to the center of the world 
        if (Input.GetKey(KeyCode.Space))
        {
            transform.position = (Vector3.zero);
        }
    }
}
